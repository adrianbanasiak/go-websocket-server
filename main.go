package main

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type SocketHandler struct {
	estabilishedConnections chan<- *websocket.Conn
	closedConnections       chan<- *websocket.Conn
	receivedMessage         chan<- []byte
}

func (s *SocketHandler) websocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)

	log.Printf("%s has connected!\n", conn.RemoteAddr())
	estabilishedConnections <- conn

	if err != nil {
		log.Println(err)
		return
	}

	for {
		msgType, msg, err := conn.ReadMessage()

		if err != nil {
			if _, ok := err.(*websocket.CloseError); ok {
				log.Printf("%s has disconnected\n", conn.RemoteAddr())

				s.closedConnections <- conn
				return
			}

			log.Printf("Read error: %e", err)
		}

		switch msgType {
		case -1:
			continue
		case 1:
			s.receivedMessage <- msg
		}
	}
}

var estabilishedConnections chan *websocket.Conn
var closedConnections chan *websocket.Conn
var receivedMessages chan []byte

const ListenPort = 3000

func main() {
	log.Println("server is starting!")

	estabilishedConnections = make(chan *websocket.Conn)
	closedConnections = make(chan *websocket.Conn)
	receivedMessages = make(chan []byte)

	s := &SocketHandler{
		estabilishedConnections: estabilishedConnections,
		closedConnections:       closedConnections,
		receivedMessage:         receivedMessages,
	}

	go connectionsManager(estabilishedConnections, closedConnections, receivedMessages)

	server := http.NewServeMux()

	server.HandleFunc("/ws", s.websocketHandler)

	log.Printf("listening on port :%d", ListenPort)
	http.ListenAndServe(fmt.Sprintf(":%d", ListenPort), server)
}

func connectionsManager(estabilishedConnections <-chan *websocket.Conn, closedConnections <-chan *websocket.Conn, messagesReceived <-chan []byte) {
	log.Println("starting connections manager")
	socketConnections := make([]*websocket.Conn, 0)

	for {
		select {
		case con := <-estabilishedConnections:
			socketConnections = append(socketConnections, con)
			broadcast(socketConnections, fmt.Sprintf("%s has connected", con.RemoteAddr()))
		case con := <-closedConnections:
			socketConnections = disconnect(socketConnections, con)
		case msg := <-messagesReceived:
			{
				log.Printf("received message %s", uuid.New().String())
				broadcast(socketConnections, string(msg))
			}
		case <-time.Tick(1 * time.Minute):
			log.Printf("currently connected %d users", len(socketConnections))
		}
	}
}

func disconnect(socketConnections []*websocket.Conn, con *websocket.Conn) []*websocket.Conn {
	for i, connection := range socketConnections {
		if connection != con {
			continue
		}

		socketConnections = append(socketConnections[:i], socketConnections[i+1:]...)
		broadcast(socketConnections, fmt.Sprintf("%s has disconnected. Farewell!", con.RemoteAddr()))
	}
	return socketConnections
}

func broadcast(connections []*websocket.Conn, msg string) {
	for _, con := range connections {
		go func(con *websocket.Conn, msg string) {
			con.WriteMessage(1, []byte(msg))
		}(con, msg)
	}
}

package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"os"
)

func main() {
	dialer := &websocket.Dialer{}

	con, _, err := dialer.Dial("ws://localhost:3000/ws", nil)

	if err != nil {
		fmt.Printf("failed to estabilish WS connection: %e\n", err)
		os.Exit(1)
	}

	for {
		msgType, msg, _ := con.ReadMessage()
		fmt.Printf("%d - %s\n", msgType, msg)
	}

}
